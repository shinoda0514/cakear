﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ModelsController : MonoBehaviour {
    public GameObject[] TargetImages;
    public GameObject[] ModelsA;
    public GameObject[] ModelsB;
    public GameObject[] ModelsC;

    private static int iTrackingObjQuan;
    public Text tt;
    public GameObject model;


    // Use this for initialization
    void Start () {
        iTrackingObjQuan = 6;
    }
	
	// Update is called once per frame
	void Update () {

        if (!isTrackingObj()) SwitchToA();
        tt.text = iTrackingObjQuan.ToString();
    }
    //return true when tracking any target image
    bool isTrackingObj() 
    {
        if (iTrackingObjQuan == 0) return false;
        else return true;
    }
    public void SwitchToA()
    {
        for (int i = 0; i < ModelsA.Length; i++) ModelsA[i].SetActive(true);
        for (int i = 0; i < ModelsB.Length; i++) ModelsB[i].SetActive(false);
        for (int i = 0; i < ModelsC.Length; i++) ModelsC[i].SetActive(false);
    }
    public void SwitchToB()
    {
        for (int i = 0; i < ModelsA.Length; i++) ModelsA[i].SetActive(false);
        for (int i = 0; i < ModelsB.Length; i++) ModelsB[i].SetActive(true);
        for (int i = 0; i < ModelsC.Length; i++) ModelsC[i].SetActive(false);
    }
    public void SwitchToC()
    {
        for (int i = 0; i < ModelsA.Length; i++) ModelsA[i].SetActive(false);
        for (int i = 0; i < ModelsB.Length; i++) ModelsB[i].SetActive(false);
        for (int i = 0; i < ModelsC.Length; i++) ModelsC[i].SetActive(true);
    }
    public void FoundTrackableObj()
    {
        iTrackingObjQuan++;        
    }
    public void LostTrackableObj()
    {
        iTrackingObjQuan--;
    }
}
