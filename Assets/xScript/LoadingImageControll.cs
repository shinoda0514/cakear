﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingImageControll : MonoBehaviour {
    Animator animatior;
	// Use this for initialization
	void Start () {
        animatior = this.GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
        bool isAnimationPlayed = (animatior.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1);
        if (isAnimationPlayed) this.gameObject.SetActive(false);
	}
}
