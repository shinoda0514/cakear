﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class ARcontroller : MonoBehaviour
{
    [HideInInspector]
    public int maxShowingCake;
    [HideInInspector]
    public int showingCakeCount;
    public GameObject[] CakeModels;
    public bool showByButton;
    public void buttonShow()
    {
        showByButton = !showByButton;
        if (showByButton)
        {
            for (int i = 0; i < CakeModels.Length+1; i++)
            {
                CakeModels[i].SetActive(true);
            }
        }
        else
        {
            for (int i = 0; i < CakeModels.Length + 1; i++)
            {
                CakeModels[i].SetActive(false);
            }
        }
    }
    private void Start()
    {
        maxShowingCake = 2;

        
    }
    // Update is called once per frame
    void Update()
    {
        CamFocus();
        CountShowingCake();
    }      
    void CountShowingCake()
    {
        int count = 0;
        for(int i = 0; i < 6; i++)
        {
            if (CakeModels[i].activeInHierarchy) count++;
        }
        showingCakeCount = count;
    }
    void CamFocus()
    {
#if UNITY_EDITOR
        if (Input.GetMouseButtonUp(0))
#elif UNITY_ANDROID || UNITY_IPHONE
            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began) 
#endif
        {
            Vuforia.CameraDevice.Instance.SetFocusMode(Vuforia.CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
        }
    }
}
