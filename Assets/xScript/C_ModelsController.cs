﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class C_ModelsController : MonoBehaviour
{
    public GameObject[] TargetImages;
    public GameObject[] ModelsA;
    public GameObject[] ModelsB;
    public GameObject[] ModelsC;

    private static int iTrackingObjQuan;
    public Text tt;

    //------手指旋轉------
    //紀錄手指觸碰位置
    Vector2 m_lastTouchedPos = new Vector2();    

    //旋轉速度
    float desktopRotateSpeed = 50.0f;
    float mobileRotateSpeed = 50.0f;
    //------------------
    int scanObjCount;//記錄掃圖的次數
    public GameObject ShowModelTip;
    bool onceFlag;
    public UnityEvent OnAllTrackableObjLost;
    // Use this for initialization
    void Start()
    {
        if(OnAllTrackableObjLost==null) OnAllTrackableObjLost = new UnityEvent();
        //從Home開始，設定12；從AR開始，設定6。不要問我為什麼
        iTrackingObjQuan = 12;
        onceFlag = false;
        scanObjCount = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (!onceFlag)
        {
            onceFlag = true;
            this.gameObject.SetActive(false);
        }
        if (!isTrackingObj()) OnAllTrackableObjLost.Invoke();
        if (ShowModelTip.activeInHierarchy)
        {
            if (scanObjCount >= 2)
            {
                ShowModelTip.SetActive(false);
            }
        }
        tt.text = scanObjCount.ToString();
        //DeskopInput();
        MobileInput();
    }
    
    //return true when tracking any target image
    bool isTrackingObj()
    {
        if (iTrackingObjQuan == 0) return false;
        else return true;
    }
    public void SwitchToA()
    {
        for (int i = 0; i < ModelsA.Length; i++) ModelsA[i].SetActive(true);
        for (int i = 0; i < ModelsB.Length; i++) ModelsB[i].SetActive(false);
        for (int i = 0; i < ModelsC.Length; i++) ModelsC[i].SetActive(false);
    }
    public void SwitchToB()
    {
        for (int i = 0; i < ModelsA.Length; i++) ModelsA[i].SetActive(false);
        for (int i = 0; i < ModelsB.Length; i++) ModelsB[i].SetActive(true);
        for (int i = 0; i < ModelsC.Length; i++) ModelsC[i].SetActive(false);
    }
    public void SwitchToC()
    {
        for (int i = 0; i < ModelsA.Length; i++) ModelsA[i].SetActive(false);
        for (int i = 0; i < ModelsB.Length; i++) ModelsB[i].SetActive(false);
        for (int i = 0; i < ModelsC.Length; i++) ModelsC[i].SetActive(true);
    }
    public void FoundTrackableObj()
    {
        iTrackingObjQuan++;
        scanObjCount++;
    }
    public void LostTrackableObj()
    {
        iTrackingObjQuan--;
    }
    void DeskopInput()
    {
        //紀錄滑鼠左鍵的移動距離
        float mx = Input.GetAxis("Mouse X");
        float my = Input.GetAxis("Mouse Y");

        if (mx != 0 || my != 0)
        {
            //滑鼠左鍵
            if (Input.GetMouseButton(0))
            {
                if (mx < 0)//往右滑
                {
                    for (int i = 0; i < ModelsA.Length; i++) ModelsA[i].transform.rotation *= Quaternion.Euler(0.0f, -mx * Time.deltaTime * desktopRotateSpeed, 0.0f);
                    for (int i = 0; i < ModelsB.Length; i++) ModelsB[i].transform.rotation *= Quaternion.Euler(0.0f, -mx * Time.deltaTime * desktopRotateSpeed, 0.0f);
                    for (int i = 0; i < ModelsC.Length; i++) ModelsC[i].transform.rotation *= Quaternion.Euler(0.0f, -mx * Time.deltaTime * desktopRotateSpeed, 0.0f);
                    //rotMedel.transform.rotation *= Quaternion.Euler(0.0f, -mx * Time.deltaTime * desktopRotateSpeed, 0.0f);
                }
                else if (mx > 0)//往左滑
                {
                    for (int i = 0; i < ModelsA.Length; i++) ModelsA[i].transform.rotation *= Quaternion.Euler(0.0f, -mx * Time.deltaTime * desktopRotateSpeed, 0.0f);
                    for (int i = 0; i < ModelsB.Length; i++) ModelsB[i].transform.rotation *= Quaternion.Euler(0.0f, -mx * Time.deltaTime * desktopRotateSpeed, 0.0f);
                    for (int i = 0; i < ModelsC.Length; i++) ModelsC[i].transform.rotation *= Quaternion.Euler(0.0f, -mx * Time.deltaTime * desktopRotateSpeed, 0.0f);
                    //rotMedel.transform.rotation *= Quaternion.Euler(0.0f, -mx * Time.deltaTime * desktopRotateSpeed, 0.0f);
                }
            }
        }
    }

    void MobileInput()
    {
        if (Input.touchCount <= 0)
            return;

        //1個手指觸碰螢幕
        if (Input.touchCount == 1)
        {

            //開始觸碰
            if (Input.touches[0].phase == TouchPhase.Began)
            {

                //紀錄觸碰位置
                m_lastTouchedPos = Input.touches[0].position;

                //手指移動
            }
            else if (Input.touches[0].phase == TouchPhase.Moved)
            {
                //旋轉物體
                //rotMedel.transform.rotation *= Quaternion.Euler(0.0f, -Input.touches[0].deltaPosition.x * Time.deltaTime * mobileRotateSpeed, 0.0f);
                float deltaX = Input.touches[0].position.x - m_lastTouchedPos.x;
                for (int i = 0; i < ModelsA.Length; i++) ModelsA[i].transform.rotation *= Quaternion.Euler(0.0f, -deltaX * Time.deltaTime * mobileRotateSpeed, 0.0f);
                for (int i = 0; i < ModelsB.Length; i++) ModelsB[i].transform.rotation *= Quaternion.Euler(0.0f, -deltaX * Time.deltaTime * mobileRotateSpeed, 0.0f);
                for (int i = 0; i < ModelsC.Length; i++) ModelsC[i].transform.rotation *= Quaternion.Euler(0.0f, -deltaX * Time.deltaTime * mobileRotateSpeed, 0.0f);
                //rotMedel.transform.rotation *= Quaternion.Euler(0.0f, -deltaX * Time.deltaTime * mobileRotateSpeed, 0.0f);
                m_lastTouchedPos = Input.touches[0].position;
            }


            //手指離開螢幕
            if (Input.touches[0].phase == TouchPhase.Ended && Input.touches[0].phase == TouchPhase.Canceled)
            {

                Vector2 pos = Input.touches[0].position;

                //手指水平移動
                if (Mathf.Abs(m_lastTouchedPos.x - pos.x) > Mathf.Abs(m_lastTouchedPos.y - pos.y))
                {
                    if (m_lastTouchedPos.x > pos.x)
                    {
                        //手指向左滑動
                    }
                    else
                    {
                        //手指向右滑動
                    }
                }
                else
                {
                    if (m_lastTouchedPos.y > pos.y)
                    {
                        //手指向下滑動
                    }
                    else
                    {
                        //手指向上滑動
                    }
                }
            }
            //攝影機縮放，如果1個手指以上觸碰螢幕
        }
        else if (Input.touchCount > 1)
        {

            //記錄兩個手指位置
            Vector2 finger1 = new Vector2();
            Vector2 finger2 = new Vector2();

            //記錄兩個手指移動距離
            Vector2 move1 = new Vector2();
            Vector2 move2 = new Vector2();

            //是否是小於2點觸碰
            for (int i = 0; i < 2; i++)
            {
                UnityEngine.Touch touch = UnityEngine.Input.touches[i];

                if (touch.phase == TouchPhase.Ended)
                    break;

                if (touch.phase == TouchPhase.Moved)
                {
                    //每次都重置
                    float move = 0;

                    //觸碰一點
                    if (i == 0)
                    {
                        finger1 = touch.position;
                        move1 = touch.deltaPosition;
                        //另一點
                    }
                    else
                    {
                        finger2 = touch.position;
                        move2 = touch.deltaPosition;

                        //取最大X
                        if (finger1.x > finger2.x)
                        {
                            move = move1.x;
                        }
                        else
                        {
                            move = move2.x;
                        }

                        //取最大Y，並與取出的X累加
                        if (finger1.y > finger2.y)
                        {
                            move += move1.y;
                        }
                        else
                        {
                            move += move2.y;
                        }

                        //當兩指距離越遠，Z位置加的越多，相反之
                        //Camera.main.transform.Translate(0, 0, move * Time.deltaTime);
                    }
                }
            }//end for
        }//end else if 
    }//end void
}
