﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShaderAlphaTest : MonoBehaviour {

	// Use this for initialization
	void Start () {
        this.GetComponent<Renderer>().material.color =new Color(GetComponent<Renderer>().material.color.r, GetComponent<Renderer>().material.color.g, GetComponent<Renderer>().material.color.b, 0.1f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
