﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TipController : MonoBehaviour {
    bool hasTrackedObj;
    public GameObject TipObj;
    public GameObject[] TargetImages;
    // Use this for initialization
    void Start () {
        hasTrackedObj = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (!hasTrackedObj)
        {
            if (isTrackingObj())//第一次掃到東西
            {
                TipObj.SetActive(false);
                hasTrackedObj = true;
            }
        }        
    }
    bool isTrackingObj()
    {
        for (int i = 0; i < TargetImages.Length; i++)
        {
            if (TargetImages[i].activeInHierarchy) return true;
        }
        return false;
    }
}
