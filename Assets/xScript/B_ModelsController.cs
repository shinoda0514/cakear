﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class B_ModelsController : MonoBehaviour
{
    public GameObject[] TargetImages;
    public GameObject[] ModelsA;
    public GameObject[] ModelsB;
    public GameObject[] ModelsC;

    private Animator modelsAnimator;

    private static int iTrackingObjQuan;
    public Text tt;
    int ii;
    bool onceFlag;
    public UnityEvent OnAllTrackableObjLost;
    // Use this for initialization
    void Start()
    {
        if (OnAllTrackableObjLost == null) OnAllTrackableObjLost = new UnityEvent();
        iTrackingObjQuan = 6;
        modelsAnimator = this.GetComponent<Animator>();
        ii = 0;
        onceFlag = false;
        //this.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (!onceFlag)
        {
            onceFlag = true;
            this.gameObject.SetActive(false);
        }
        if (!isTrackingObj()) OnAllTrackableObjLost.Invoke();
        //tt.text = iTrackingObjQuan.ToString();
        tt.text = ii.ToString();
    }

    //return true when tracking any target image
    bool isTrackingObj()
    {
        if (iTrackingObjQuan == 0) return false;
        else return true;
    }
    public void SwitchToA()
    {
        for (int i = 0; i < ModelsA.Length; i++) ModelsA[i].SetActive(true);
        for (int i = 0; i < ModelsB.Length; i++) ModelsB[i].SetActive(false);
        for (int i = 0; i < ModelsC.Length; i++) ModelsC[i].SetActive(false);
    }
    public void SwitchToB()
    {
        for (int i = 0; i < ModelsA.Length; i++) ModelsA[i].SetActive(false);
        for (int i = 0; i < ModelsB.Length; i++) ModelsB[i].SetActive(true);
        for (int i = 0; i < ModelsC.Length; i++) ModelsC[i].SetActive(false);
    }
    public void SwitchToC()
    {
        for (int i = 0; i < ModelsA.Length; i++) ModelsA[i].SetActive(false);
        for (int i = 0; i < ModelsB.Length; i++) ModelsB[i].SetActive(false);
        for (int i = 0; i < ModelsC.Length; i++) ModelsC[i].SetActive(true);
    }
    public void FoundTrackableObj()
    {
        iTrackingObjQuan++;
        //ResetAllModels();
        modelsAnimator.SetTrigger("StartRotate");
        ii++;
    }
    public void LostTrackableObj()
    {
        iTrackingObjQuan--;
        modelsAnimator.SetTrigger("StopRotate");
    }
    void ResetAllModels()
    {
        for (int i = 0; i < ModelsA.Length; i++) ModelsA[i].transform.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
        for (int i = 0; i < ModelsB.Length; i++) ModelsB[i].transform.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
        for (int i = 0; i < ModelsC.Length; i++) ModelsC[i].transform.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
    }
}